import React from 'react'
import Header from '../components/Header'
import ErrorLoadingRenderer from '../components/ErrorLoadingRenderer'
import ClickGame from '../components/ClickGame'

export default function Click() {
  const loading = false, error = false

  return (
    <>
      <Header
        title={<><span className="highlight">Crypto</span>Click</>}
        subtitle={<>Get some <span className="highlight">coin</span> by clicking!</>}
      />
      <ErrorLoadingRenderer loading={loading} error={error}>
        <ClickGame />
      </ErrorLoadingRenderer>
    </>
  )
}
