import {useState} from 'react'

export default function useClicker() {
  const [points, setPoints] = useState(0)

  const clickButton = () => {
    setPoints((prevState => prevState + 1))
  }

  return {
    points,
    clickButton
  }
}