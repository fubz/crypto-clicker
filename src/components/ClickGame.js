import React from 'react'
import useClicker from '../hooks/useClicker.hook'

export default function ClickGame() {
  const { points, clickButton } = useClicker()

  return (
    <div>
      <ClickerMessage points={points} />
      <ClickButton clickButton={clickButton} />
      <div style={{ margin: '2em' }}>
        <>
          Points <span className="highlight">{points}</span>
        </>
      </div>
    </div>
  )
}

const ClickButton = ({ clickButton }) => (
  <div onClick={clickButton} className="btn btn-bordered btn-light btn-clicker">
    <i className="ri-ghost-smile-fill btn-icon" /> Click me
  </div>
)

const ClickerMessage = ({ points }) => {
  return (
    <span>
      {points < 100 ? (
        <>
          <i className="ri-play-line" />
          Start by clicking to 100
        </>
      ) : (
        <>
          <i className="ri-thumb-up-line" /> Good Job.... that's about it
        </>
      )}
    </span>
  )
}
